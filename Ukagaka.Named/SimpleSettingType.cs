﻿namespace Ukagaka.Named
{
	/// <summary>
	/// キー値ペア形式設定ファイル種類の列挙です
	/// </summary>
	public enum SimpleSettingType
	{
		/// <summary>
		/// 種類はありません
		/// </summary>
		None = 0,
		/// <summary>
		/// ゴースト
		/// </summary>
		Ghost = 1,
		/// <summary>
		/// ゴーストへの追加ファイル
		/// </summary>
		Supplement = 2,
		/// <summary>
		/// バルーン
		/// </summary>
		Balloon = 3,
		/// <summary>
		/// プラグイン
		/// </summary>
		Plugin = 4,
		/// <summary>
		/// ヘッドラインセンサ
		/// </summary>
		Headline = 5,
		/// <summary>
		/// カレンダーのスキン
		/// </summary>
		CalendarSkin = 6,
		/// <summary>
		/// カレンダーのスケジュールセンサ
		/// </summary>
		CalendarPlugin = 7,
		/// <summary>
		/// 複数パッケージ
		/// </summary>
		Package = 8,
		/// <summary>
		/// 不明
		/// </summary>
		Unknown = -1,
	}
}
